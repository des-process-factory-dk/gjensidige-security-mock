package no.gjensidige.springboot.common.security.tomcat;

import javax.servlet.http.HttpServletRequest;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

public class TAIPlusPreAuthFilter extends AbstractPreAuthenticatedProcessingFilter {
    public TAIPlusPreAuthFilter() {
    }

    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
        return request.getUserPrincipal();
    }

    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        return request.getRemoteUser();
    }
}

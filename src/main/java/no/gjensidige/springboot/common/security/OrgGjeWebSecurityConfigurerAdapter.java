package no.gjensidige.springboot.common.security;

import no.gjensidige.springboot.common.security.tomcat.TAIAuthenticationUserDetailsService;
import no.gjensidige.springboot.common.security.tomcat.TAIPlusPreAuthFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer.AuthorizedUrl;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;

public class OrgGjeWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
    public OrgGjeWebSecurityConfigurerAdapter() {
    }

    private TAIPlusPreAuthFilter taiPlusPreAuthFilter() throws Exception {
        TAIPlusPreAuthFilter preAuthFilter = new TAIPlusPreAuthFilter();
        preAuthFilter.setAuthenticationManager(super.authenticationManagerBean());
        return preAuthFilter;
    }

    private PreAuthenticatedAuthenticationProvider preAuthenticatedAuthenticationProvider() {
        PreAuthenticatedAuthenticationProvider preAuthenticationProvider = new PreAuthenticatedAuthenticationProvider();
        preAuthenticationProvider.setPreAuthenticatedUserDetailsService(new TAIAuthenticationUserDetailsService());
        return preAuthenticationProvider;
    }

    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(this.preAuthenticatedAuthenticationProvider());
    }

    protected void configure(HttpSecurity http) throws Exception {
        this.csrf(http);
        this.cors(http);
        this.tokenAuthentication(http);
        this.authorizeRequests(http);
        this.otherConfigurations(http);
    }

    protected void csrf(HttpSecurity http) throws Exception {
        http.csrf().disable();
    }

    protected void cors(HttpSecurity http) throws Exception {
        http.cors();
    }

    protected void tokenAuthentication(HttpSecurity http) throws Exception {
        http.addFilter(this.taiPlusPreAuthFilter());
    }

    protected void authorizeRequests(HttpSecurity http) throws Exception {
        ((AuthorizedUrl)http.authorizeRequests().anyRequest()).denyAll();
    }

    protected void otherConfigurations(HttpSecurity http) {
    }
}

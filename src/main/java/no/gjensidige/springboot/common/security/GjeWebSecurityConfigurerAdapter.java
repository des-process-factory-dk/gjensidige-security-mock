package no.gjensidige.springboot.common.security;

import no.gjensidige.springboot.common.security.tomcat.TAIAuthenticationUserDetailsService;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AnonymousAuthenticationFilter;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;

import java.util.Collection;
import java.util.LinkedList;

@Configuration
@EnableWebSecurity
public class GjeWebSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		super.configure(http);
	}

	public AnonymousAuthenticationFilter taiPlusPreAuthFilter()  {
		return new NopFilter() {
		};
	}


	private PreAuthenticatedAuthenticationProvider preAuthenticatedAuthenticationProvider() {
		PreAuthenticatedAuthenticationProvider preAuthenticationProvider = new PreAuthenticatedAuthenticationProvider();
		preAuthenticationProvider.setPreAuthenticatedUserDetailsService(new TAIAuthenticationUserDetailsService());
		return preAuthenticationProvider;
	}

	protected void configure(AuthenticationManagerBuilder auth) {
		auth.authenticationProvider(this.preAuthenticatedAuthenticationProvider());
	}

	protected void tokenAuthentication(HttpSecurity http) throws Exception {
		http.addFilter(this.taiPlusPreAuthFilter());
	}

	protected void authorizeRequests(HttpSecurity http) throws Exception {
		((ExpressionUrlAuthorizationConfigurer.AuthorizedUrl)http.authorizeRequests().anyRequest()).denyAll();
	}


}
